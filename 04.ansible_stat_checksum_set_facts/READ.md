ANSIBLE - 4. MODULES STAT, CHECKSUM ET SET_FACTS


Ansible c’est bien mais il ne faut pas en abuser. Un concept essentiel à appliquer c’est l’idempotence ou la réentrance. Concrètement, on ne doit pas réaliser une action si celle-ci n’est pas nécessaire.
Dans notre exemple, nous découvrirons comment gérer un fichier déjà présent sur une machine et ayant subit des modifications pour atteindre un état final. Si celui-ci a déjà atteint sont état final, il n’est pas nécessaire d’appliquer les actions initiales.
Pour cela, nous allons:
1 - calculer le md5sum d’un fichier avec le module stat
2 - ajouter une variable dans une tâche avec set_fact pour gérer les différents cas de figure (variable définie ou non).
3 - créer un bloc avec le module block pour continuer les actions à mener avec when.

Gérer la réentrance sur un fichier modifié :
1 - module stat : informations sur un fichier
2 - module debug : afficher le contenu d’une variable (register)
3 - set_fact et when : définir une variable dans une tache
4 - module block : conditionner une ensemble de taches

